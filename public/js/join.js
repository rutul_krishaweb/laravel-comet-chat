$(document).ready(function(){

  jQuery(function(){

    var join = {
      init: function() {
        var appID = "332521fe408663b";
        var region = "us";
        var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region).build();
        CometChat.init(appID, appSetting).then(
          () => {
            console.log("Initialization completed successfully");
          },
          error => {
            console.log("Initialization failed with error:", error);
          }
          );
      },

      chatInit: function() {

          console.log(uid);
          
          CometChatWidget.init({
              "appID": "332521fe408663b",
              "appRegion": "us",
              "authKey": "300d61914cc69b27093f9db22ce1d26879509e56"
            }).then(response => {
              console.log("Initialization completed successfully");
              CometChatWidget.login({
                "uid": uid
              }).then(response => {
                CometChatWidget.launch({
                  "widgetID": "bc4ee6a3-7464-4e1b-bcb8-5a6df18d6372",
                  "target": "#cometchat",
                  "roundedCorners": "true",
                  "height": "600px",
                  "width": "600px",
                  "defaultID": guid, 
                  "defaultType": 'group' 
                });
              }, error => {
                console.log("User login failed with error:", error);
              });
            }, error => {
              console.log("Initialization failed with error:", error);
            });
      },

      login: function() {
        CometChat.login(authToken).then(
          User => {
            console.log("Login successfully:", { User });

            let deafaultLayout = true;
            let sessionID = session_id;
            let audioOnly = false;
            // console.log(sessionID);
            // console.log(User.role);

            var listenerID = User.uid;
            // CometChat.addConnectionListener(
            //   listenerID,
            //   new CometChat.ConnectionListener({
            //     onConnected: () => {
            //       console.log("ConnectionListener => On Connected");
            //     },
            //     inConnecting: () => {
            //       console.log("ConnectionListener => In connecting");
            //     },
            //     onDisconnected: () => {
            //       console.log("ConnectionListener => On Disconnected");
            //     }
            //   })
            //   );


            if(User.role == 'student')
            { 
              // console.log('student if');
              // let callSettings = new CometChat.CallSettingsBuilder()
              // .enableDefaultLayout(deafaultLayout)
              // .setSessionID(sessionID)
              // .setIsAudioOnlyCall(audioOnly)
              // .showPauseVideoButton(false)
              // .showMuteAudioButton(true)
              // .build();
              // join.joinCall(callSettings);
            }
            else
            {
                // console.log('tutor else');
                // let callSettings = new CometChat.CallSettingsBuilder()
                // .enableDefaultLayout(deafaultLayout)
                // .setSessionID(sessionID)
                // .setIsAudioOnlyCall(audioOnly)
                // .build();
                // join.joinCall(callSettings);
                if (!has_dashboard_url) {
                  join.dashboard();
                }

            }

            // let CSS=`
            // .custom-main-container{
            //   border: 1px solid #F0F0F0;
            // }
            // .custom--tile-main-call-container{
            //   border: 2px solid #6929CA;
            // }
            // .custom-tile-main-video-container{
            //   border: 2px solid yellow;
            // }
            // .custom-spotlight-main-video-container{
            //   border: 2px solid orange;
            // }
            // .custom-tile-list-container{
            //   width: 40%; 
            //   border: 2px solid #FFFFFF;
            // }
            // .custom-tile-container{
            //   height: 80%;
            //   border: 2px solid pink;
            // }
            // .custom-local-spotlight-stream-container{
            //   border: 2px solid #FFFFFF;
            // }
            // .custom-button-bar{
            //   border: 2px solid red;
            // }
            // .custom-button {
            //   border: 2px solid green;
            // }
            // .custom-grid-container{
            //   background-color: blue;
            // }
            // .custom-name{
            //   color:grey;
            //   font-weight:bold;
            //   border: 1px solid #F0F0F0;
            // }`
            let callSettings = new CometChat.CallSettingsBuilder()
              .setSessionID(sessionID)
              .setIsAudioOnlyCall(false)
              .showEndCallButton(true)
              .enableDefaultLayout(false)
              .showScreenShareButton(true)
              .showMuteAudioButton(true)
              .showEndCallButton(true)
              .showScreenShareButton(true)
              .showPauseVideoButton(true)
              // .setCustomCSS(CSS)
              .build();
              join.joinCall(callSettings);
            
          },
          error => {
            console.log("Login failed with exception:", { error });
          }
          );
      },

      joinCall: function(callSettings){

        // console.log('join call');
        console.log(callSettings);
        CometChat.startCall(
          callSettings,
          document.getElementById("callScreen"),
          console.log('start call'),
          new CometChat.OngoingCallListener({
            onUserJoined: user => {
                console.log("User joined call:", user);
            },
            onUserListUpdated: userList => {
              console.log("user list:", userList);
            },
            onCallEnded: call => {
               window.location.href = '/groups';
              console.log("Call ended:", call);
            },
            onError: error => {
              console.log("Error :", error);
            }
          })
          );

      },

      dashboard: function(){
        CometChat.callExtension("whiteboard", "POST", "v1/create", { 
                "receiver": guid,
                "receiverType": "group"
             }).then(response => {
                // Response with board_url
                var board_url = response.board_url;
                $.ajax({
                    type: 'POST',
                    url: '/store-dashboard-url',
                    data: { 'guid' : guid ,'board_url' : board_url},   
                })
                .done(function(response) {
                   $('#white-board').attr('src', board_url);
                   console.log(response);
                })
                .fail(function(response) {
                     console.log(response);
                });

            }).catch(error => {
                // Some error occured
                console.log('error');
        });

        // CometChat.callExtension("document", "POST", "v1/create", { 
        //     "receiver": guid,
        //     "receiverType": "group"
        // }).then(response => {
        //     console.log(response);
        // }).catch(error => {
        //     // Some error occured
        // });   

      },

      connection :function(listener_id){

        // var listenerID = listener_id;
        // CometChat.addConnectionListener(
        //   listenerID,
        //   new CometChat.ConnectionListener({
        //     onConnected: () => {
        //       console.log("ConnectionListener => On Connected");
        //     },
        //     inConnecting: () => {
        //       console.log("ConnectionListener => In connecting");
        //     },
        //     onDisconnected: () => {
        //       console.log("ConnectionListener => On Disconnected");
        //     }
        //   })
        //   );
      }

    }

    join.init();
    join.chatInit();
    // join.login();
    // $('.screen').on('click', function(){

    //   let callController = CometChat.CallController.getInstance();
    //   callController.startScreenShare();
    // });
    //  $('.mute').on('click', function(){

    //    let callController = CometChat.CallController.getInstance();
    //    console.log(callController);
    //     callController.muteAudio(true);
    // });
   
  });

});
