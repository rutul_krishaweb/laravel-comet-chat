$(document).ready(function(){

  jQuery(function(){

    var comet = {
      init: function() {
        var appID = "3150309389abe28";
        var region = "us";
        var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region).build();
        CometChat.init(appID, appSetting).then(
          () => {
            console.log("Initialization completed successfully");
          },
          error => {
            console.log("Initialization failed with error:", error);
          }
          );
      },
      login: function() {
        CometChat.login(authToken).then(
          User => {
            console.log("Login successfully:", { User });
          },
          error => {
            console.log("Login failed with exception:", { error });
          }
          );
      },

      initiateCall: function() {
        var receiverID = guid;
        var callType = CometChat.CALL_TYPE.VIDEO;
        var receiverType = CometChat.RECEIVER_TYPE.GROUP;

        var call = new CometChat.Call(receiverID, callType, receiverType);

        CometChat.initiateCall(call).then(
          outGoingCall => {
            alert('call initiated');
            console.log("Call initiated successfully:", outGoingCall);
            localStorage.setItem('sessionId',outGoingCall.sessionId);
          // perform action on success. Like show your calling screen.
          },
          error => {
            console.log("Call initialization failed with exception:", error);
          }
          );

          // var connectionStatus = CometChat.getConnectionStatus();
          // console.log(connectionStatus);
      },

      callListener: function(){

          var listnerID = uid;
          alert(listnerID);
          CometChat.addCallListener(
              listnerID,
          new CometChat.CallListener({
          onIncomingCallReceived(call) {
            console.log("Incoming call:", call);
            // Handle incoming call
          },
          onOutgoingCallAccepted(call) {
            console.log("Outgoing call accepted:", call);
             var sessionId = call.sessionId;
                var callType = call.type;
                var callSettings = new CometChat.CallSettingsBuilder()
                .setSessionID(sessionId)
                .enableDefaultLayout(true)
                .setIsAudioOnlyCall(callType == 'audio' ? true : false)
                .build();
                CometChat.startCall(
                  callSettings,
                  document.getElementById("callScreen"),
                  new CometChat.OngoingCallListener({
                    onUserJoined: user => {
                      /* Notification received here if another user joins the call. */
                      alert('user Joined');
                      console.log("User joined call:", user);
                      /* this method can be use to display message or perform any actions if someone joining the call */
                    },
                    onUserLeft: user => {
                      /* Notification received here if another user left the call. */
                      console.log("User left call:", user);
                      /* this method can be use to display message or perform any actions if someone leaving the call */
                    },
                    onUserListUpdated: userList => {
                      console.log("user list:", userList);
                    },
                    onCallEnded: call => {
                      /* Notification received here if current ongoing call is ended. */
                      console.log("Call ended:", call);
                      /* hiding/closing the call screen can be done here. */
                    },
                    onError: error => {
                      console.log("Error :", error);
                      /* hiding/closing the call screen can be done here. */
                    }
                  })
                  );
              // Outgoing Call Accepted
          },
          onOutgoingCallRejected(call) {
            console.log("Outgoing call rejected:", call);
          // Outgoing Call Rejected
          },
          onIncomingCallCancelled(call) {
            console.log("Incoming call calcelled:", call);
          }
          })
        );

          // CometChat.removeCallListener(listnerID);
      },

      acceptCall: function(){

          var sessionID = "1617716884de96c8b6bfa38ac89e97deb73649b7703e8f0098";

          CometChat.acceptCall(sessionID).then(
              call => {
                console.log("Call accepted successfully:", call);
               var sessionId = call.sessionId;
                var callType = call.type;
                var callSettings = new CometChat.CallSettingsBuilder()
                .setSessionID(sessionId)
                .enableDefaultLayout(true)
                .setIsAudioOnlyCall(callType == 'audio' ? true : false)
                .build();
                CometChat.startCall(
                  callSettings,
                  document.getElementById("callScreen"),
                  new CometChat.OngoingCallListener({
                    onUserJoined: user => {
                      /* Notification received here if another user joins the call. */
                      alert('user Joined');
                      console.log("User joined call:", user);
                      /* this method can be use to display message or perform any actions if someone joining the call */
                    },
                    onUserLeft: user => {
                      /* Notification received here if another user left the call. */
                      console.log("User left call:", user);
                      /* this method can be use to display message or perform any actions if someone leaving the call */
                    },
                    onUserListUpdated: userList => {
                      console.log("user list:", userList);
                    },
                    onCallEnded: call => {
                      /* Notification received here if current ongoing call is ended. */
                      console.log("Call ended:", call);
                      /* hiding/closing the call screen can be done here. */
                    },
                    onError: error => {
                      console.log("Error :", error);
                      /* hiding/closing the call screen can be done here. */
                    }
                  })
                  );
               // start the call using the startCall() method
          },
          error => {
            console.log("Call acceptance failed with error", error);
          // handle exception
          }
          );
      },

      rejectCall: function(){

      }
}

    comet.init();
    comet.login();
    $('.initiate-call').on('click', function() {
      comet.initiateCall();
      comet.callListener();
    });
    $('.accept-call').on('click', function() {
      comet.acceptCall();
      comet.callListener();
    });
});

});
