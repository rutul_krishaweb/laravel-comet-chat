<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GroupsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/dashboard', [DashboardController::class, 'dashboard'])->middleware(['auth'])->name('dashboard');

Route::get('/join/{group_slug}/{session_id}', [GroupsController::class, 'join'])->middleware(['auth'])->name('join');

Route::get('/join-groups', [GroupsController::class, 'join'])->middleware(['auth'])->name('join-groups');

Route::post('/store-dashboard-url', [GroupsController::class, 'storeDashboardUrl'])->middleware(['auth'])->name('store-dashboard-url');

// Route::delete('/leave-group', [DashboardController::class, 'leaveGroup'])->middleware(['auth'])->name('leave-group');

Route::resource('groups', GroupsController::class)->middleware(['auth']);

require __DIR__.'/auth.php';
