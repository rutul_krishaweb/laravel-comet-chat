<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Former;
use Session;
use Auth;
use App\Models\Group;

class GroupsController extends Controller
{

    public function index()
    {
        $groups = Group::all();

        if (Auth::user()->role == 'tutor') {
            return view('groups.tutor-index', compact('groups'));
        } else {
            return view('groups.student-index', compact('groups'));
        }
    }

    public function create()
    {
        return view('groups.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required|max:500',
        ]);

        if ($validator->fails()) {
            Former::withErrors($validator);
            return redirect()->back()->withErrors($validator)->withInput()->withError('Please correct following errors!');
        }
        try {
            $random = \Str::random(40);
            $group = new Group;
            $group->user_id = Auth::user()->id;
            $group->name = $request->name;
            $group->description = $request->description;
            $group->save();

            $group = Group::find($group->id);
            $group->join_link = route('join', [$group->slug, $random]);
            $group->save();

            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL            => "https://api-us.cometchat.io/v2.0/groups",
                CURLOPT_POST           => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS     => array(
                    'name' => $group->name,
                    'guid' => $group->slug,
                    'type' => 'public',
                    'description' => $group->description,
                    'owner' => Auth::user()->slug
                ),
                CURLOPT_HTTPHEADER => array(
                    'appId: 332521fe408663b',
                    'apiKey: a77166f434b93838baac315ce972f1e11116889a',
                )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

            return redirect()->route('groups.index')->withSuccess('Group created successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withError('Something went wrong, Please try after sometime!');
        }
    }

    public function show($group_id)
    {
        try {
            $group = Group::find($group_id);

            //show old
            // $user = Auth::user();

            // $ch = curl_init();
            // $curlConfig = array(
            //     CURLOPT_URL            => "https://api-us.cometchat.io/v2.0/users/".$user->slug."/groups/".$group->slug."/members",
            //     CURLOPT_POST           => true,
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_HTTPHEADER => array(
            //         'appId: 332521fe408663b',
            //         'apiKey: a77166f434b93838baac315ce972f1e11116889a',
            //     )
            // );
            // curl_setopt_array($ch, $curlConfig);
            // $result = curl_exec($ch);
            // $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            // curl_close($ch);
            // if ($httpcode == 417) {
            //     return redirect()->back()->withError('Something went wrong, Please try after sometime!');
            //     print('You have already joined group!');
            // } else {
            //     return view('groups.show', compact('group'));
            // }
            return view('groups.show', compact('group'));
        } catch (\Exception $e) {
            return redirect()->back()->withError('Something went wrong, Please try after sometime!');
        }
    }

    public function join($group_slug, $session_id)
    {
        try {
            $group = Group::where('slug', $group_slug)->first();
            return view('groups.join', compact('group', 'session_id'));
        } catch (\Exception $e) {
            return redirect()->back()->withError('Something went wrong, Please try after sometime!');
        }
    }

    public function edit($group_id)
    {
        $group = Group::find($group_id);
        return view('groups.edit', compact('group'));
    }

    public function update(Request $request, $group_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required|max:500',
        ]);

        if ($validator->fails()) {
            Former::withErrors($validator);
            return redirect()->back()->withErrors($validator)->withInput()->withError('Please correct following errors!');
        }
        try {
            $group = Group::find($group_id);
            $group->name = $request->name;
            $group->description = $request->description;
            $group->save();

            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL            => "https://api-us.cometchat.io/v2.0/groups/" .$group->slug,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST  => "PUT",
                CURLOPT_POSTFIELDS     => http_build_query(array(
                    'name' => $request->name,
                    // 'description' => $group->description,
                )),
                CURLOPT_HTTPHEADER => array(
                    'appId: 332521fe408663b',
                    'apiKey: a77166f434b93838baac315ce972f1e11116889a',
                )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

            return redirect()->route('groups.index')->withSuccess('Group created successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withError('Something went wrong, Please try after sometime!');
        }
    }

    public function destroy($group_id)
    {
        try {
            $group = Group::find($group_id);
            $group->delete();

            $ch = curl_init();
            $curlConfig = array(
                CURLOPT_URL  => "https://api-us.cometchat.io/v2.0/groups/" .$group->slug,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST  => "DELETE",
                CURLOPT_HTTPHEADER => array(
                    'appId: 332521fe408663b',
                    'apiKey: a77166f434b93838baac315ce972f1e11116889a',
                )
            );
            curl_setopt_array($ch, $curlConfig);
            $result = curl_exec($ch);
            curl_close($ch);

            return redirect()->route('groups.create')->withSuccess('Group created successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->withError('Something went wrong, Please try after sometime!');
        }
    }

    public function storeDashboardUrl(Request $request)
    {
        try {
            $group = Group::where('slug', $request->guid)->first();
            $group->board_url = $request->board_url;
            $group->save();

            return response()->json(["success" => "Inserted successfully."], 200);
        } catch (\Exception $e) {
            return response()->json(["error" => "Something went wrong, Please try after sometime."], 422);
        }
    }
}
