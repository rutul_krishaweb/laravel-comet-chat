<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Group;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    // public function leaveGroup($group_id)
    // {
    //     try {
    //         $group = Group::find($group_id);
    //         $user = Auth::user();

    //         $ch = curl_init();
    //         $curlConfig = array(
    //             CURLOPT_URL            => "https://api-us.cometchat.io/v2.0/users/".$user->slug."/groups/".$group->slug."/members",
    //             CURLOPT_RETURNTRANSFER => true,
    //             CURLOPT_CUSTOMREQUEST  => "DELETE",
    //             CURLOPT_HTTPHEADER => array(
    //                 'appId: 3150309389abe28',
    //                 'apiKey: 3dfb97158bf0493f9541b18a5325da198aa75435',
    //             )
    //         );
    //         curl_setopt_array($ch, $curlConfig);
    //         $result = curl_exec($ch);
    //         curl_close($ch);

    //         return redirect()->route('dashboard')->withSuccess('Left Group successfully!');
    //     } catch (\Exception $e) {
    //         return redirect()->back()->withError('Something went wrong, Please try after sometime!');
    //     }
    // }
}
