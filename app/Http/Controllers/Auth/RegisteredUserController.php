<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        // Auth::login($user = User::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password),
        //     'role' => $request->role
        // ]));

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role
        ]);

        // $comet_api_key = config('comet.API_KEY');
        // $comet_app_id = config('comet.API_ID');

        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL            => "https://api-us.cometchat.io/v2.0/users",
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array(
                'name' => $user->name,
                'uid' => $user->slug,
                'role' => $user->role,
                'withAuthToken' => true
            ),
            CURLOPT_HTTPHEADER => array(
                'apiKey: a77166f434b93838baac315ce972f1e11116889a',
                'appId: 332521fe408663b',
            )
        );
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);

        $token = json_decode($result, true)['data']['authToken'];
        $user->auth_token = $token ;
        $user->save();
        event(new Registered($user));

        // return redirect(RouteServiceProvider::HOME);
        return redirect()->route('login');
    }
}
