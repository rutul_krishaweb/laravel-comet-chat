<html>

<head>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <script defer src="https://widget-js.cometchat.io/v2/cometchatwidget.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <img src="{!! asset('/images/logo.png') !!}"/>  
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{!! route('login') !!}">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{!! route('register') !!}">Register</a>
            </li>
        </ul>
    </nav>
    {{-- <div id="cometchat"></div> --}}
    {{-- <script>
        window.addEventListener('DOMContentLoaded', (event) => {
            CometChatWidget.init({
                "appID": "3150309389abe28",
                "appRegion": "us",
                "authKey": "bf85ffe16dd5b207a4fe7316c1d09f7823ea5255"
            }).then(response => {
                console.log("Initialization completed successfully");
                //You can now call login function.
                CometChatWidget.login({
                    "uid": "superhero1"
                }).then(response => {
                    CometChatWidget.launch({
                        "widgetID": "0dbad388-a9e7-43e2-b8a0-51c7604d3461",
                        "target": "#cometchat",
                        "roundedCorners": "true",
                        "height": "600px",
                        "width": "800px",
                        "defaultID": 'superhero1', //default UID (user) or GUID (group) to show,
                        "defaultType": 'user' //user or group
                    });
                }, error => {
                    console.log("User login failed with error:", error);
                    //Check the reason for error and take appropriate action.
                });
            }, error => {
                console.log("Initialization failed with error:", error);
                //Check the reason for error and take appropriate action.
            });
        });
    </script> --}}
</body>
</html>