<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
           Groups
        </h2>
    </x-slot>

    <div class="py-12">
        {{-- <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    You're logged in!
                </div>
            </div>
        </div> --}}

        <div class="py-12">
            <div class="container">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{!! route('groups.create') !!}" class="float-right btn btn-outline-primary">Add Group</a>
                    <table class="table">
                       <thead>
                          <th>Name</th>
                          <th>Action</th>
                      </thead>
                      <tbody>
                        @foreach($groups as $group)
                        @php 
                            $random = \Str::random(40);
                        @endphp
                        <tr>
                            <td>{!! $group->name !!}</td>
                            <td> 
                                <a href="{!! route('groups.show',$group->id) !!}" class="btn-sm btn-success">Join Group</a>
                                <a href="{!! route('groups.edit',$group->id) !!}" class="btn-sm btn-primary">Edit Group</a>
                                <a href="javascript:;" class="join" data-url="{!! route('join',[$group->slug, $random])  !!}">{!! route('join',[$group->slug, $random]) !!}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>

        {{-- <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <a href="{!! route('groups.show',5) !!}" class="btn-sm btn-success">Join Group</a>
        </div> --}}
    </div>
  @section('scripts')
      <script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.3.0/CometChat.js"></script>
      <script defer src="https://widget-js.cometchat.io/v2/cometchatwidget.js"></script>
      <script type="text/javascript">
        $('.join').on('click', function() {
            var url = $(this).data('url');
            window.location.href = url;
        });
      </script>
  @endsection
</x-app-layout>
