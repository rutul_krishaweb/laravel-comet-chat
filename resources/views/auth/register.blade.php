<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                {{-- <x-application-logo class="w-20 h-20 fill-current text-gray-500" /> --}}
                <img src="{!! asset('/images/logo.png') !!}"/>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">

            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')"  autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"  />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                 autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation"  />
            </div>
            
            <div class="block mt-4">
                <x-label value="Role" />
                <label for="role" class="mt-1 mr-2 inline-flex items-center">
                    <input id="role" type="radio" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="role" value="student" checked>
                    <span class="ml-2 text-sm text-gray-600">Student</span>
                </label>
                <label for="role" class="inline-flex items-center">
                    <input id="role" type="radio" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="role" value="tutor">
                    <span class="ml-2 text-sm text-gray-600">Tutor</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4" id="submit">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
{{-- @section('scripts')
<script type="text/javascript">
    $('#submit').on('click',function(e){
        e.preventDefault();
        var _this = jQuery(this);
        var _f = _this.parents('form');

        $.ajax({
            type: _f.attr('method'),
            url: _f.attr('action'),
            data: _f.serialize(),        
        })
        .done(function(response) {
            // ajax.success(response)
            //create User
            // let apiKey = "3dfb97158bf0493f9541b18a5325da198aa75435";
            // var uid = "rutul";
            // var name = "Rutul";

            // var user = new CometChat.User(uid);

            // user.setName(name);

            // CometChat.createUser(user, apiKey).then(
            //     user => {
            //         console.log("user created", user);
            //     },error => {
            //         console.log("error", error);
            //     }
            console.log('done');
        })
        .fail(function(response) {
            // ajax.error(response)
            console.log('fail');
        });
    });
</script>
@endsection --}}
</x-guest-layout>
