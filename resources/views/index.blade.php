<!DOCTYPE html>
<html>
<head>
   <script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.3.0/CometChat.js"></script>
   <script defer src="https://widget-js.cometchat.io/v2/cometchatwidget.js"></script>
</head>
<body> 

    <iframe src="https://whiteboard-embed-us.cometchat.io?whiteboardid=eyJhcHBJZCI6IjMxNTAzMDkzODlhYmUyOCIsInJlY2VpdmVyIjoiZ3JvdXBfMTYxNzE4MjgwMDg4OSIsInJlY2VpdmVyVHlwZSI6Imdyb3VwIiwidGltZXN0YW1wIjoxNjE3MTk0MzUwLCJpYXQiOjE2MTcxOTQzNTB9~sT_09gTMPiIom9P7Le0xNn1zQIputYEiDeP7A4GLSe0&accesstoken=board" width="500px" height="400px" frameBorder="0" style="border: 0;"></iframe>
    {{-- <br>Brought to you by <a href="" target="_blank"></a> --}}
    <div id="cometchat"></div>
   

    <a href=" https://document-embed-us.cometchat.io/p/eyJhcHBJZCI6IjMxNTAzMDkzODlhYmUyOCIsInJlY2VpdmVyIjoiZ3JvdXBfMTYxNzE4MjgwMDg4OSIsInJlY2VpdmVyVHlwZSI6Imdyb3VwIiwidGltZXN0YW1wIjoxNjE3MTk1MTQyLCJpYXQiOjE2MTcxOTUxNDJ9~2JVC0UCL47-O55O58-_NwLyHseRW_vjmxKT_iqtNQBg" title=""><img src="" width="100%" style="max-width: 850px;" alt="">Document</a><br>Provided by <a href="" target="_blank"></a>


    <script type="text/javascript">

        window.addEventListener('DOMContentLoaded', (event) => {
        CometChatWidget.init({
            "appID": "3150309389abe28",
            "appRegion": "us",
            "authKey": "bf85ffe16dd5b207a4fe7316c1d09f7823ea5255"
        }).then(response => {
            console.log("Initialization completed successfully");
            //You can now call login function.
            CometChatWidget.login({
                "uid": "superhero1"
            }).then(response => {
                CometChatWidget.launch({
                    "widgetID": "0dbad388-a9e7-43e2-b8a0-51c7604d3461",
                    "target": "#cometchat",
                    "roundedCorners": "true",
                    "height": "600px",
                    "width": "200px",
                    "defaultID": 'test', //default UID (user) or GUID (group) to show,
                    "defaultType": 'group' //user or group
                });
            }, error => {
                console.log("User login failed with error:", error);
                //Check the reason for error and take appropriate action.
            });
        }, error => {
            console.log("Initialization failed with error:", error);
            //Check the reason for error and take appropriate action.
        });
    });

        var appID = "3150309389abe28";
        var region = "us";
        var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region).build();
        CometChat.init(appID, appSetting).then(
          () => {
            console.log("Initialization completed successfully");
        },
        error => {
            console.log("Initialization failed with error:", error);
        }
        );


        //create User
        // let apiKey = "3dfb97158bf0493f9541b18a5325da198aa75435";
        // var uid = "rutul";
        // var name = "Rutul";

        // var user = new CometChat.User(uid);

        // user.setName(name);

        // CometChat.createUser(user, apiKey).then(
        //     user => {
        //         console.log("user created", user);
        //     },error => {
        //         console.log("error", error);
        //     }
        //     )

        //Login User
        // var UID = "rutul";
        // var apiKey = "3dfb97158bf0493f9541b18a5325da198aa75435";

        // CometChat.login(UID, apiKey).then(
        //   user => {
        //     console.log("Login Successful:", { user });    
        // },
        // error => {
        //     console.log("Login failed with exception:", { error });    
        // }
        // );

        //create auth token
        // const options = {
        //   method: 'POST',
        //   headers: {
        //     appId: '3150309389abe28',
        //     apiKey: '3dfb97158bf0493f9541b18a5325da198aa75435',
        //     'Content-Type': 'application/json',
        //     Accept: 'application/json'
        // },
        //     body: JSON.stringify({force: false})
        // };

        // fetch('https://api-us.cometchat.io/v2.0/users/rutul/auth_tokens', options)
        // .then(response => console.log(response))
        // .catch(err => console.error(err));

        //join memeber to group
        // let GUID = "group_1617182800889";
        // let membersList = [
        //     new CometChat.GroupMember("rutul", CometChat.GROUP_MEMBER_SCOPE.PARTICIPANT),
        // ];

        // CometChat.addMembersToGroup(GUID, membersList, []).then(
        //   response => {
        //     console.log("response", response);
        // },
        // error => {
        //     console.log("Something went wrong", error);
        // }
        // );


        var authToken = "superhero1_1617194314f44090b620ee86c861fc163ff7c3fa";

        CometChat.login(authToken).then(
          User => {
            console.log("Login successfully:", { User });

            // CometChat.callExtension("whiteboard", "POST", "v1/create", { 
            //     "receiver": "group_1617182800889",
            //     "receiverType": "group"
            //  }).then(response => {
            //     // Response with board_url
            //     console.log(response);
            // }).catch(error => {
            //     // Some error occured
            //     console.log('error');
            // });

            // CometChat.callExtension("document", "POST", "v1/create", { 
            //     "receiver": "group_1617182800889",
            //     "receiverType": "group"
            // }).then(response => {
            //     console.log(response);
            // }).catch(error => {
            //     // Some error occured
            // });

            // CometChat.callExtension('broadcast', 'POST', 'v1/broadcast', {
            //   receiverType: "group",
            //   receiver: "group_1617182800889"
            // }).then(response => {
            //     // Success response
            //     console.log(response);
            // }).catch(error => {
            //     // Some error occured
            // });
          
          },
          error => {
            console.log("Login failed with exception:", { error });
            // User login failed, check error and take appropriate action.
          }
        );


        // CometChat.callExtension("whiteboard", "POST", "v1/create", { 
        //     "receiver": "group_1617182800889",
        //     "receiverType": "group"
        // }).then(response => {
        //         // Response with board_url
        //         console.log('success');
        //     }).catch(error => {
        //         // Some error occured
        //         console.log('error');
        //     });
    </script>

</body>
</html>