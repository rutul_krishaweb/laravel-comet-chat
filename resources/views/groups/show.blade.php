
<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      Group Details
  </h2>
</x-slot>

<div class="py-12">
    <div class="container">
        <div class="table-responsive">
          <table class="table">
            <tbody>
              <tr><th style="width: 90px;">Group Name</th><td>{!! $group->name !!}</td></tr>
              <tr><th style="width: 90px;">Group Description</th><td>{!! $group->description !!}</td></tr>
              <tr><th style="width: 90px;">Join Link</th><td><a href="{!! $group->join_link !!}" target="_blank">{!! $group->join_link !!} </a></td></tr>
            </tbody>
         </table>
        </div>
    </div>
</div>
</x-app-layout>