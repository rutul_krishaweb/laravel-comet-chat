
<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Dashboard') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="container">
      <div class="row">
        {{-- <div class="col-xs-6 col-md-6">
             <div class="callScreen" id="callScreen"></div>
        </div> --}}
     {{--    <div class="col-xs-6 col-md-6">
          <iframe id="white-board" src="{!! $group->board_url !!}" width="100%" height="600px" frameBorder="0" style="border: 0;"></iframe>
        </div> --}}
       {{--  <a href="https://document-embed-us.cometchat.io/p/eyJhcHBJZCI6IjMxNTAzMDkzODlhYmUyOCIsInJlY2VpdmVyIjoibWF0aHMiLCJyZWNlaXZlclR5cGUiOiJncm91cCIsInRpbWVzdGFtcCI6MTYxNzc5MjM5NiwiaWF0IjoxNjE3NzkyMzk2fQ~FnpbUGY5pTvazPKT2fB1yqTtg-zACnoWx2tfcEw2hFw" >Document</a> --}}
      {{--  <EMBED SRC="https://document-embed-us.cometchat.io/p/eyJhcHBJZCI6IjMxNTAzMDkzODlhYmUyOCIsInJlY2VpdmVyIjoibWF0aHMiLCJyZWNlaXZlclR5cGUiOiJncm91cCIsInRpbWVzdGFtcCI6MTYxNzc5MjM5NiwiaWF0IjoxNjE3NzkyMzk2fQ~FnpbUGY5pTvazPKT2fB1yqTtg-zACnoWx2tfcEw2hFw" VOLUME="50" HEIGHT="200" WIDTH="144"> --}}
        {{-- <a href="javascript:;" class="screen">Screen Share</a> --}}
        <div class="col-xs-6 col-md-4">
            <div id="cometchat"></div>
        </div>
        {{-- <a href="javascript:;" class="mute">Mute Audio</a> --}}
        {{-- 2.3.2 --}}
      </div>

    <br/>
    </div>
  </div>
@section('scripts')
  <script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.3.0/CometChat.js"></script>
  <script defer src="https://widget-js.cometchat.io/v2/cometchatwidget.js"></script>
  {{-- <script type="text/javascript" src="{!! asset('js/CometChat.js') !!}"></script>
  <script src="{!! asset('/js/cometchatwidget.js') !!}"></script> --}}
  <script type="text/javascript">
    var guid = "{!! $group->slug !!}";
    var uid = "{!! Auth::user()->slug !!}";
    var authToken = "{!! Auth::user()->auth_token  !!}";
    var session_id = "{!! $session_id !!}";
    var has_dashboard_url = {!! isset($group->board_url) ? 'true' : 'false' !!};
  </script>
  <script src="{!! asset('/js/join.js') !!}"></script>
 {{--  <script type="text/javascript">
        // CometChat.callExtension("whiteboard", "POST", "v1/create", { 
        //         "receiver": guid,
        //         "receiverType": "group"
        //      }).then(response => {
        //         // Response with board_url
        //         console.log(response);
        //     }).catch(error => {
        //         // Some error occured
        //         console.log('error');
        // });
  </script> --}}
@endsection
</x-app-layout>