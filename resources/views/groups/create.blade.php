<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      Create Group
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="container">
      <form method="POST" action="{!! route('groups.store') !!}">
        @csrf
        <div class="form-group">
          <label for="exampleInputName">Name</label>
          <input type="text" name="name" class="form-control" id="exampleInputName" aria-describedby="textHelp" placeholder="Enter name">
           @error('name')
                  <span class="help">{{ $message }}</span>
           @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputDescription">Description</label>
          <textarea name="description" class="form-control" id="exampleInputDescription"></textarea>
         @error('description')
                <span class="help">{{ $message }}</span>
         @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</x-app-layout>