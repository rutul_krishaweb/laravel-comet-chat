<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      Update Group
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="container">
      <form method="POST" action="{!! route('groups.update', $group->id) !!}">
        @method('patch')
        @csrf
        <div class="form-group">
          <label for="exampleInputName">Name</label>
          <input type="text" name="name" class="form-control" id="exampleInputName" aria-describedby="textHelp" placeholder="Enter name" value="{!! $group->name !!}">
           @error('name')
                <span class="help">{{ $message }}</span>
           @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputDescription">Description</label>
          <textarea name="description" class="form-control" id="exampleInputDescription">{!! $group->description !!}</textarea>
           @error('description')
                <span class="help">{{ $message }}</span>
           @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
    {{-- <form method="POST" action="{{ route('groups.destroy',$group->id) }}">
      @method('delete')
      @csrf
      <a href="{{ route('groups.destroy',$group->id) }}" class="float-right btn btn-outline-danger" onclick="event.preventDefault();
      this.closest('form').submit();">delete</a>
   </form> --}}
</div>
</x-app-layout>