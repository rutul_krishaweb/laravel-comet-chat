
<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Dashboard') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-md-4">
          {{-- <img src="{!! asset('images/1.jpeg') !!}" > --}}
             <div class="callScreen" id="callScreen"></div>
        </div>
        <div class="col-xs-6 col-md-4">
          <iframe src="https://whiteboard-embed-us.cometchat.io?whiteboardid=eyJhcHBJZCI6IjMxNTAzMDkzODlhYmUyOCIsInJlY2VpdmVyIjoibWF0aHMiLCJyZWNlaXZlclR5cGUiOiJncm91cCIsInRpbWVzdGFtcCI6MTYxNzYxNDIzMiwiaWF0IjoxNjE3NjE0MjMyfQ~9Oc0WdJuB9_5vmYZ3oWQZBxnRbMS9la9lYmuH2IIOj8&accesstoken=board" width="100%" height="600px" frameBorder="0" style="border: 0;"></iframe>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="cometchat"></div>
          {{-- <img src="{!! asset('images/1.jpeg') !!}" > --}}
        </div>
      </div>

    <br/>
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="javascript:;" class=" btn-sm btn-success initiate-call"> Initiate Call </a>
      </div>
      <br/>

      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="javascript:;" class=" btn-sm btn-success accept-call"> Accept Call </a>
      </div>
      <br/>

       <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="javascript:;" class=" btn-sm btn-success reject-call"> Reject Call </a>
      </div>

      {{-- <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="{!! route('leave-group',$group->id) !!}" class="btn-sm btn-success">Leave Group</a>
      </div> --}}
    </div>
  </div>
  @section('scripts')
  <script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.3.0/CometChat.js"></script>
  <script defer src="https://widget-js.cometchat.io/v2/cometchatwidget.js"></script>
  <script type="text/javascript">
    var guid = "{!! $group->slug !!}";
    var uid = "{!! Auth::user()->slug !!}";
    var authToken = "{!! Auth::user()->auth_token  !!}"
  </script>
  <script defer src="{!! asset('/js/comet.js') !!}"></script>
  <script type="text/javascript"> 

    var guid = "{!! $group->slug !!}";
    var uid = "{!! Auth::user()->slug !!}";

    // var appID = "3150309389abe28";
    // var region = "us";
    // var appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(region).build();
    // CometChat.init(appID, appSetting).then(
    //   () => {
    //     console.log("Initialization completed successfully");
    //   },
    //   error => {
    //     console.log("Initialization failed with error:", error);
    //   }
    //   );


    // var authToken = 'rutul-thakkar-3_161761409468baad30318e19e3455fce34145485';
    // CometChat.login(authToken).then(
    //   User => {
    //     console.log("Login successfully:", { User });

    //         // CometChat.callExtension("whiteboard", "POST", "v1/create", { 
    //         //     "receiver": guid,
    //         //     "receiverType": "group"
    //         //  }).then(response => {
    //         //     // Response with board_url
    //         //     console.log('hi');
    //         //     console.log(response);
    //         // }).catch(error => {
    //         //     // Some error occured
    //         //     console.log('error');
    //         // });

    //         // CometChat.callExtension("document", "POST", "v1/create", { 
    //         //     "receiver": "group_1617182800889",
    //         //     "receiverType": "group"
    //         // }).then(response => {
    //         //     console.log(response);
    //         // }).catch(error => {
    //         //     // Some error occured
    //         // });


    //         // var receiverID = guid;
    //         // var messageText = "Hello world!";
    //         // var receiverType = CometChat.RECEIVER_TYPE.GROUP;

    //         // var textMessage = new CometChat.TextMessage(
    //         //   receiverID,
    //         //   messageText,
    //         //   receiverType
    //         // );

    //         // CometChat.sendMessage(textMessage).then(
    //         //   message => {
    //         //     console.log("Message sent successfully:", message);
    //         //   },
    //         //   error => {
    //         //     console.log("Message sending failed with error:", error);
    //         //   }
    //         // );

    //         // var receiverID = guid;
    //         // var callType = CometChat.CALL_TYPE.VIDEO;
    //         // var receiverType = CometChat.RECEIVER_TYPE.GROUP;

    //         // var call = new CometChat.Call(receiverID, callType, receiverType);

    //         // CometChat.initiateCall(call).then(
    //         //   outGoingCall => {
    //         //     console.log("Call initiated successfully:", outGoingCall);
    //         //   // perform action on success. Like show your calling screen.
    //         // },
    //         // error => {
    //         //   console.log("Call initialization failed with exception:", error);
    //         // }
    //         // );

    //         // CometChat.acceptCall("1617621832e78f36d110a59dfbac5f0b3a2adff5aa7f91006c").then(
    //         //   call => {
    //         //     console.log("Call accepted successfully:", call);
    //         //     var sessionId = call.sessionId;
    //         //     var callType = call.type;
    //         //     var callSettings = new CometChat.CallSettingsBuilder()
    //         //     .setSessionID(sessionId)
    //         //     .enableDefaultLayout(true)
    //         //     .setIsAudioOnlyCall(callType == 'audio' ? true : false)
    //         //     .build();
    //         //     CometChat.startCall(
    //         //       callSettings,
    //         //       document.getElementById("callScreen"),
    //         //       new CometChat.OngoingCallListener({
    //         //         onUserJoined: user => {
    //         //           /* Notification received here if another user joins the call. */
    //         //           console.log("User joined call:", user);
    //         //           /* this method can be use to display message or perform any actions if someone joining the call */
    //         //         },
    //         //         onUserLeft: user => {
    //         //          // Notification received here if another user left the call. 
    //         //          console.log("User left call:", user);
    //         //          /* this method can be use to display message or perform any actions if someone leaving the call */
    //         //        },
    //         //        onUserListUpdated: userList => {
    //         //         console.log("user list:", userList);
    //         //       },
    //         //       onCallEnded: call => {
    //         //         /* Notification received here if current ongoing call is ended. */
    //         //         console.log("Call ended:", call);
    //         //         /* hiding/closing the call screen can be done here. */
    //         //       },
    //         //       onError: error => {
    //         //         console.log("Error :", error);
    //         //         /* hiding/closing the call screen can be done here. */
    //         //       }
    //         //     })
    //         //       );
    //         //   // start the call using the startCall() method
    //         // },
    //         // error => {
    //         //   console.log("Call acceptance failed with error", error);
    //         //   // handle exception
    //         // }
    //         // );

    //       },
    //       error => {
    //         console.log("Login failed with exception:", { error });
    //         // User login failed, check error and take appropriate action.
    //       }
    //       );


    window.addEventListener('DOMContentLoaded', (event) => {
      CometChatWidget.init({
        "appID": "3150309389abe28",
        "appRegion": "us",
        "authKey": "bf85ffe16dd5b207a4fe7316c1d09f7823ea5255"
      }).then(response => {
        console.log("Initialization completed successfully");
                //You can now call login function.
                CometChatWidget.login({
                  "uid": uid
                }).then(response => {
                  CometChatWidget.launch({
                    "widgetID": "0dbad388-a9e7-43e2-b8a0-51c7604d3461",
                    "target": "#cometchat",
                    "roundedCorners": "true",
                    "height": "600px",
                    "width": "600px",
                        "defaultID": guid, //default UID (user) or GUID (group) to show,
                        "defaultType": 'group' //user or group
                      });
                }, error => {
                  console.log("User login failed with error:", error);
                    //Check the reason for error and take appropriate action.
                  });
              }, error => {
                console.log("Initialization failed with error:", error);
                //Check the reason for error and take appropriate action.
              });
    });
</script>
@endsection
</x-app-layout>